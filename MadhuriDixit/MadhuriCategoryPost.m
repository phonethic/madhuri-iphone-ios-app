//
//  MadhuriCategoryPost.m
//  MadhuriDixit
//
//  Created by Rishi Saxena on 04/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "MadhuriCategoryPost.h"

@implementation MadhuriCategoryPost

@synthesize categoryPostId=_categoryPostId;
@synthesize categoryPostType=_categoryPostType;
@synthesize categoryPostSlug=_categoryPostSlug;
@synthesize categoryPostUrl=_categoryPostUrl;
@synthesize categoryPostStatus=_categoryPostStatus;
@synthesize categoryPostTitle=_categoryPostTitle;
@synthesize categoryPostTitlePlain=_categoryPostTitlePlain;
@synthesize categoryPostContent=_categoryPostContent;
@synthesize categoryPostDate=_categoryPostDate;
@synthesize categoryPostExcerpt=_categoryPostExcerpt;
@synthesize categoryPostModified=_categoryPostModified;
@synthesize categoryPostThumbnail=_categoryPostThumbnail;

#pragma mark NSCoding Protocol

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeInt32:self.categoryPostId forKey:@"categoryPostId"];
    [coder encodeObject:self.categoryPostType forKey:@"categoryPostType"];
    [coder encodeObject:self.categoryPostSlug forKey:@"categoryPostSlug"];
    [coder encodeObject:self.categoryPostUrl forKey:@"categoryPostUrl"];
    [coder encodeObject:self.categoryPostStatus forKey:@"categoryPostStatus"];
    [coder encodeObject:self.categoryPostTitle forKey:@"categoryPostTitle"];
    [coder encodeObject:self.categoryPostTitlePlain forKey:@"categoryPostTitlePlain"];
    [coder encodeObject:self.categoryPostContent forKey:@"categoryPostContent"];
    [coder encodeObject:self.categoryPostExcerpt forKey:@"categoryPostExcerpt"];
    [coder encodeObject:self.categoryPostDate forKey:@"categoryPostDate"];
    [coder encodeObject:self.categoryPostModified forKey:@"categoryPostModified"];
    [coder encodeObject:self.categoryPostThumbnail forKey:@"categoryPostThumbnail"];
}


- (id)initWithCoder:(NSCoder *)coder {
	self = [super init];
	if (self != nil) {
        self.categoryPostId         = [coder decodeInt32ForKey:@"categoryPostId"];
        self.categoryPostType       = [coder decodeObjectForKey:@"categoryPostType"];
        self.categoryPostSlug       = [coder decodeObjectForKey:@"categoryPostSlug"];
        self.categoryPostUrl        = [coder decodeObjectForKey:@"categoryPostUrl"];
        self.categoryPostStatus     = [coder decodeObjectForKey:@"categoryPostStatus"];
        self.categoryPostTitle      = [coder decodeObjectForKey:@"categoryPostTitle"];
        self.categoryPostTitlePlain = [coder decodeObjectForKey:@"categoryPostTitlePlain"];
        self.categoryPostContent    = [coder decodeObjectForKey:@"categoryPostContent"];
        self.categoryPostExcerpt    = [coder decodeObjectForKey:@"categoryPostExcerpt"];
        self.categoryPostDate       = [coder decodeObjectForKey:@"categoryPostDate"];
        self.categoryPostModified   = [coder decodeObjectForKey:@"categoryPostModified"];
        self.categoryPostThumbnail  = [coder decodeObjectForKey:@"categoryPostThumbnail"];
        
	}
	return self;
}

@end
