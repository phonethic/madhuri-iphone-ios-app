//
//  MadhuriMoviesViewController.h
//  MadhuriDixit
//
//  Created by Rishi on 17/09/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class MadhuriMovies;

@interface MadhuriMoviesViewController : UIViewController<NSXMLParserDelegate,AVAudioPlayerDelegate> {
    int status;
    MadhuriMovies *moviesObj;
    NSXMLParser *xmlParser;
    NSMutableData *responseAsyncData;
     BOOL goBack;
    
    BOOL isDragging;
    BOOL isLoading;
    
    AVAudioPlayer *sectionSoundAudioPlayer;
}
@property (strong, nonatomic) IBOutlet UIImageView *loaderImage;
@property (strong, nonatomic) IBOutlet UITableView *moviesTableView;
@property (strong, nonatomic) NSMutableArray *moviesArray;

@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;

- (void)setupStrings;
- (void)addPullToRefreshHeader;
- (void)startLoading;
- (void)stopLoading;

@end
