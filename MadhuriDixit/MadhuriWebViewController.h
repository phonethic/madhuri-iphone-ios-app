//
//  MadhuriWebViewController.h
//  MadhuriDixit
//
//  Created by Rishi Saxena on 04/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MadhuriWebViewController : UIViewController <UIWebViewDelegate,UIActionSheetDelegate>

@property (nonatomic, readwrite) NSInteger categoryId;
@property (nonatomic, copy) NSString *posttitle;
@property (nonatomic, copy) NSString *postLink;
@property (unsafe_unretained, nonatomic) IBOutlet UIWebView *contentview;
@property (strong, nonatomic) IBOutlet UIImageView *navBarImg;
@property (strong, nonatomic) IBOutlet UIImageView *loader_image;
@property (strong, nonatomic) IBOutlet UIButton *facebookshareBtn;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barbackBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barfwdBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barrefreshBtn;
@property (strong, nonatomic) IBOutlet UIToolbar *webviewtoolBar;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)webviewbackBtnPressed:(id)sender;
- (IBAction)webviewfwdBtnPressed:(id)sender;
- (IBAction)webviewrefreshBtnPressed:(id)sender;

@end
