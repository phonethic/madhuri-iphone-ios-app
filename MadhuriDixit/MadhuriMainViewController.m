//
//  MadhuriMainViewController.m
//  MadhuriDixit
//
//  Created by Rishi Saxena on 06/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "MadhuriMainViewController.h"
#import "MadhuriHealthBeautyViewController.h"
#import "MadhuriLatestViewController.h"
#import "MadhuriDanceViewController.h"
#import "MadhuriArtViewController.h"
#import "MadhuriFashionViewController.h"
#import "MadhuriMusicViewController.h"
#import "MadhuriAppDelegate.h"

@interface MadhuriMainViewController ()

@end

@implementation MadhuriMainViewController
@synthesize latestBtnPressed;
@synthesize danceBtnPressed;
@synthesize artBtnPressed;
@synthesize fashionBtnPressed;
@synthesize healthBtnPressed;
@synthesize musicBtnPresswd;
@synthesize networkSwitchPressed;
@synthesize networkstatus;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self roundButtonWithBorder:self.latestBtnPressed];
    [self roundButtonWithBorder:self.danceBtnPressed];
    [self roundButtonWithBorder:self.artBtnPressed];
    [self roundButtonWithBorder:self.fashionBtnPressed];
    [self roundButtonWithBorder:self.healthBtnPressed];
    [self roundButtonWithBorder:self.musicBtnPresswd];
    [myAppDelegate saveNetworkStatus:1];
}

-(void)roundButtonWithBorder:(UIButton*)button
{
    button.layer.cornerRadius = 10;
    button.clipsToBounds = YES;
    button.layer.borderColor = [UIColor lightGrayColor].CGColor;
    button.layer.borderWidth = 1.0;
}

- (void)viewDidUnload
{
    [self setLatestBtnPressed:nil];
    [self setDanceBtnPressed:nil];
    [self setArtBtnPressed:nil];
    [self setFashionBtnPressed:nil];
    [self setHealthBtnPressed:nil];
    [self setNetworkSwitchPressed:nil];
    [self setMusicBtnPresswd:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)loadLatestView:(id)sender {
    MadhuriLatestViewController *viewController = [[MadhuriLatestViewController alloc] initWithNibName:@"MadhuriLatestViewController" bundle:nil];
    viewController.title = @"Latest";
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)loadDanceView:(id)sender {
    MadhuriDanceViewController *viewController = [[MadhuriDanceViewController alloc] initWithNibName:@"MadhuriDanceViewController" bundle:nil];
    viewController.title = @"Dance";
    [self.navigationController pushViewController:viewController animated:YES];

}

- (IBAction)loadFashionView:(id)sender {
    MadhuriFashionViewController *viewController = [[MadhuriFashionViewController alloc] initWithNibName:@"MadhuriFashionViewController" bundle:nil];
    viewController.title = @"Fashion";
    [self.navigationController pushViewController:viewController animated:YES];

}

- (IBAction)loadHealthView:(id)sender {
    MadhuriHealthBeautyViewController *viewController = [[MadhuriHealthBeautyViewController alloc] initWithNibName:@"MadhuriHealthBeautyViewController" bundle:nil];
    viewController.title = @"Health & Beauty";
    [self.navigationController pushViewController:viewController animated:YES];

}

- (IBAction)loadMusicView:(id)sender {
    MadhuriMusicViewController *viewController = [[MadhuriMusicViewController alloc] initWithNibName:@"MadhuriMusicViewController" bundle:nil];
    viewController.title = @"Music";
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)networkChanged:(id)sender {
    if (networkSwitchPressed.on) {
        [myAppDelegate saveNetworkStatus:1];
    } else {
        [myAppDelegate saveNetworkStatus:0];
    }
}

- (IBAction)loadArtView:(id)sender {
    MadhuriArtViewController *viewController = [[MadhuriArtViewController alloc] initWithNibName:@"MadhuriArtViewController" bundle:nil];
    viewController.title = @"Art";
    [self.navigationController pushViewController:viewController animated:YES];
}
@end
