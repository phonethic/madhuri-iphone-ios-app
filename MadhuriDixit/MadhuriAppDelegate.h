//
//  MadhuriAppDelegate.h
//  MadhuriDixit
//
//  Created by Rishi Saxena on 02/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"
#import <FacebookSDK/FacebookSDK.h>
#define myAppDelegate (MadhuriAppDelegate*)[[UIApplication sharedApplication] delegate] 
#define titlenotificationName  @"MadhuriTitleNotification"
#define titlenotificationKey  @"ButtonPressed"
//=CONCATENATE("<key>Name</key><string>", A1,"</string><key>Abbreviaiton</key><string>", B1,"</string><key>PubmedID</key><string>", C1,"</string><key>Impactfactor</key><string>", D1,"</string><key>Website</key><string>",E1,"</string>")
extern NSString *const SCSessionStateChangedNotification;

@class SCViewController;
@class MadhuriHealthViewController;

@interface MadhuriAppDelegate : UIResponder <UIApplicationDelegate,AVAudioPlayerDelegate> {
    Reachability* internetReach;
    Boolean networkavailable;
    AVAudioPlayer *buttonSoundAudioPlayer;
}
@property(nonatomic) Boolean networkavailable;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;

- (NSString *)filterHTMLCodes:(NSString *)String;
-(void) writeToTextFile:(NSString *)lcontent name:(NSString *)lname;
-(NSString *) getTextFromFile:(NSString *)lname;
- (void)removeFile:(NSString*)lname; 
- (void)saveImage: (UIImage*)image name:(int)lname;
- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize;
- (void)saveNetworkStatus:(int)lval;
- (int)getNetworkStatus;
-(void) playAudioOnButtonPressed;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
@end
