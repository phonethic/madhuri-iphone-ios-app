//
//  MadhuriMusicViewController.m
//  MadhuriDixit
//
//  Created by Rishi Saxena on 14/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "MadhuriMusicViewController.h"
#import "MadhuriAppDelegate.h"
#import "SBJson.h"
#import "MadhuriCategoryPost.h"
#import "MadhuriWebViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MadhuriMusicViewController ()

@end

@implementation MadhuriMusicViewController
@synthesize musicTableView;
@synthesize responseIndicator;
@synthesize musicArray;
@synthesize videodict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.responseIndicator.hidesWhenStopped = TRUE;
    [self.responseIndicator startAnimating];
	// Do any additional setup after loading the view, typically from a nib.
    if(musicArray == nil) {
        musicArray = [[NSMutableArray alloc] init];
        videodict = [[NSMutableDictionary alloc] init];
    }
    
    if([myAppDelegate getNetworkStatus])
        [self startJsonRequest];
    else 
        [self loadDataFromFile];

}


-(void) startJsonRequest
{
    NSString *urlString = [NSString stringWithFormat:@"http://madhuridixit-nene.com/wp/?json=get_page&slug=music-fanspicks&custom_fields=video_url,video_name"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:YES timeoutInterval:30.0];
    [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {	
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
        //DebugLog(@"status = %d ",status);
	}
}	

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseData==nil)
	{	
		responseData = [[NSMutableData alloc] initWithLength:0];
	}	
	
	[responseData appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    //[self logData];
    
    [self.responseIndicator stopAnimating];
    
    [self loadDataFromWeb];
    
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [self.responseIndicator stopAnimating];
    
    [self loadDataFromFile];
	
}

- (void) logData 
{
    if(responseData != NULL)
	{
		NSString *result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		//DebugLog(@"\n result:%@\n\n", result);
    }
}

-(void)loadDataFromFile
{
    NSString *responseString = [[NSString alloc] initWithString:[myAppDelegate getTextFromFile:@"Music"]];
    //[self logData];
    [self parseData:responseString];
    [musicTableView reloadData];
    responseString = nil;
    self->responseData = nil;
}

-(void)loadDataFromWeb
{
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [myAppDelegate writeToTextFile:responseString name:@"Music"];
    [self parseData:responseString];
    [musicTableView reloadData];
    responseString = nil;
    self->responseData = nil;
    
}

-(void)parseData:(NSString *)dataString
{
    //Get json value 
    NSDictionary* mainDict = (NSDictionary*)[dataString JSONValue];
    
    //Get address dictionary
    NSDictionary* pagecategory = [mainDict objectForKey:@"page"];
    NSString * lid = [pagecategory objectForKey:@"id"];
    NSString * type = [pagecategory objectForKey:@"type"];
    NSString * slug = [pagecategory objectForKey:@"slug"];
    NSString * url = [pagecategory objectForKey:@"url"];
    NSString * lstatus = [pagecategory objectForKey:@"status"];
    NSString * title = [myAppDelegate  filterHTMLCodes:[pagecategory objectForKey:@"title"]];
    NSString * title_plain = [pagecategory objectForKey:@"title_plain"];
    NSString * date = [pagecategory objectForKey:@"date"];
    NSString * modified = [pagecategory objectForKey:@"modified"];
    DebugLog(@" id %@ , type %@, slug %@, url %@, status %@, title %@, title_plain %@,  date %@, modified %@",lid,type,slug,url,lstatus,title,title_plain,date,modified);
 
    NSMutableArray *videoUrlArray = [[NSMutableArray alloc] init];

    //Get custom dictionary
    NSDictionary* customcategory = [pagecategory objectForKey:@"custom_fields"];
    NSArray *keys1 = [customcategory allKeys]; // the keys for your dictionary 
    NSEnumerator *e1 = [keys1 objectEnumerator];
    NSString *string1;
    while (string1 = [e1 nextObject]) {  
        NSArray* customArray = [customcategory objectForKey:string1];
        if([string1 isEqualToString:@"video_url"]) {
            for( NSString *videoUrl in customArray){  
                [videoUrlArray addObject:videoUrl];
                
            }
        } else if([string1 isEqualToString:@"video_name"]) {
            for( NSString *videoName in customArray){  
                [musicArray addObject:videoName];
            }
        }
    }
    
    for(int i=0;i<musicArray.count; i++)
    {
        [videodict setObject:[videoUrlArray objectAtIndex:i] forKey:[musicArray objectAtIndex:i]];
    }

    //DebugLog(@"%@", videodict);
}



#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [musicArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UILabel *lblTitle;
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        //cell.backgroundColor = [UIColor clearColor];
        UIImageView *tempImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 80)];
        [tempImg setImage:[UIImage imageNamed:@"table_row_bg.png"]];
        cell.backgroundView = tempImg;
        
        //Initialize Image View with tag 1.(Thumbnail Image)
//        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 3, 50, 50)];
//        thumbImg.tag = 1;
//        thumbImg.contentMode = UIViewContentModeScaleAspectFill;
//        thumbImg.layer.cornerRadius = 10;
//        thumbImg.layer.masksToBounds = YES;
//        thumbImg.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        thumbImg.layer.borderWidth = 1.0;
//        [cell.contentView addSubview:thumbImg];
        
        //Initialize Label with tag 2.(Title Label)
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 10, cell.frame.origin.y + 8, 220, cell.frame.size.height )];
        lblTitle.tag = 2;
        lblTitle.shadowColor   = [UIColor blackColor];
        lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblTitle.font = TABLEVIEW_CELLLABEL_FONT;
        lblTitle.textAlignment = UITextAlignmentLeft;
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblTitle];
        
        //Initialize Image View with tag 3.(Arrow Image)
       /* UIImageView *arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake(290, 24, 14, 12)];
        [arrowImg setImage:[UIImage imageNamed:@"bigarrow.png"]];
        arrowImg.tag = 3;
        [cell.contentView addSubview:arrowImg];*/
        
        
    }

//    UIImageView *thumbImgview = (UIImageView *)[cell viewWithTag:1];
//    [thumbImgview setImageWithURL:[NSURL URLWithString:catObj.categoryPostThumbnail]
//                 placeholderImage:[UIImage imageNamed:@"Icon.png"]
//                          success:^(UIImage *image) {
//                              DebugLog(@"success");
//                          }
//                          failure:^(NSError *error) {
//                              DebugLog(@"write error %@", error);
//                          }];
    
    lblTitle = (UILabel *)[cell viewWithTag:2];
    lblTitle.text = [musicArray objectAtIndex:indexPath.row];
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MadhuriWebViewController *typedetailController = [[MadhuriWebViewController alloc] initWithNibName:@"MadhuriWebViewController" bundle:nil] ;
    typedetailController.title = self.title;
    typedetailController.categoryId = indexPath.row;
    [self.navigationController pushViewController:typedetailController animated:YES];
    [musicTableView deselectRowAtIndexPath:[musicTableView indexPathForSelectedRow] animated:NO];
    
}



- (void)viewDidUnload
{
    [self setMusicTableView:nil];
    [self setResponseIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//iOS 5
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
//iOS6
- (BOOL)shouldAutorotate {
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

@end
