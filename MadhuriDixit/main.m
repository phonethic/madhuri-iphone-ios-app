//
//  main.m
//  MadhuriDixit
//
//  Created by Rishi Saxena on 02/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MadhuriAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MadhuriAppDelegate class]));
    }
}
