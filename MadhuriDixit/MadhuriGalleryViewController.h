//
//  MadhuriGalleryViewController.h
//  MadhuriDixit
//
//  Created by Rishi on 30/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MadhuriAppDelegate.h"
#import "MWPhotoBrowser.h"
#import <QuartzCore/QuartzCore.h>

@interface MadhuriGalleryViewController : UIViewController <MWPhotoBrowserDelegate>
{
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    BOOL goBack;
    int status;
    NSMutableArray *galleryPhotos;
    NSArray *_photos;
}
@property (nonatomic, retain) NSArray *photos;
@property (nonatomic, copy) NSString *dataFilePath;
@property (strong, nonatomic) IBOutlet UIView *alertView;
@property (strong, nonatomic) IBOutlet UIImageView *loaderImage;

- (IBAction)alertActionMethod:(id)sender;
- (IBAction)backActionMethod:(id)sender;

@end
