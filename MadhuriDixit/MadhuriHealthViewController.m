//
//  MadhuriViewController.m
//  MadhuriDixit
//
//  Created by Rishi Saxena on 02/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "MadhuriHealthViewController.h"
#import "SBJson.h"
#import "MadhuriCategoryPost.h"
#import "MadhuriWebViewController.h"

@interface MadhuriHealthViewController ()

@end

@implementation MadhuriHealthViewController
@synthesize responseIndicator;
@synthesize mainCategoryTable;
@synthesize categoryArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.responseIndicator.hidesWhenStopped = TRUE;
    [self.responseIndicator startAnimating];
	// Do any additional setup after loading the view, typically from a nib.
    if(categoryArray == nil) {
        categoryArray = [[NSMutableArray alloc] init];
    }
    [self startJsonRequest];

}

-(void) startJsonRequest
{
    NSString *urlString = [NSString stringWithFormat:@"http://madhuridixit-nene.com/wp/?json=get_category_posts&slug=health-beauty"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NO timeoutInterval:30.0];
    [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {	
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
        NSLog(@"status = %d ",status);
	}
}	

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseData==nil)
	{	
		responseData = [[NSMutableData alloc] initWithLength:0];
	}	
	
	[responseData appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    //[self logData];
    
    [self.responseIndicator stopAnimating];
    
    //Convert respone date in string format
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    //Get json value 
    NSDictionary* mainDict = (NSDictionary*)[responseString JSONValue];
    NSString * firstname = [mainDict objectForKey:@"status"];
	NSString * lastname = [mainDict objectForKey:@"count"];
    NSString * age = [mainDict objectForKey:@"pages"];
    NSLog(@" %@ , %@ , %@ ",firstname,lastname,age);
    
    //Get address dictionary
    NSDictionary* maincategory = [mainDict objectForKey:@"category"];
    NSArray *keys = [maincategory allKeys]; // the keys for your dictionary 
    NSEnumerator *e = [keys objectEnumerator];
    NSString *string;
    while (string = [e nextObject]) {  //Loop through all the values in dictionary
        // do something with object
        NSString * value = [maincategory objectForKey:string];
        NSLog(@"%@ = %@", string, value);
    }
    
    //Get phone number array
    NSArray* postsArray = [mainDict objectForKey:@"posts"];
    
    for(NSDictionary* phone in postsArray){  //Loop through all the dictionaries in an array
        NSString * lid = [phone objectForKey:@"id"];
        NSString * type = [phone objectForKey:@"type"];
        NSString * slug = [phone objectForKey:@"slug"];
        NSString * url = [phone objectForKey:@"url"];
        NSString * lstatus = [phone objectForKey:@"status"];
        NSString * title = [phone objectForKey:@"title"];
        NSString * title_plain = [phone objectForKey:@"title_plain"];
        NSString * content = [phone objectForKey:@"content"];
        NSString * excerpt = [phone objectForKey:@"excerpt"];
        NSString * date = [phone objectForKey:@"date"];
        NSString * modified = [phone objectForKey:@"modified"];
//        NSLog(@" id %@ , type %@, slug %@, url %@, status %@, title %@, title_plain %@, excerpt %@, date %@, modified %@",lid,type,slug,url,lstatus,title,title_plain,excerpt,date,modified);
        
        // Create a new category object with the data from the parser
        MadhuriCategoryPost *catObj = [[MadhuriCategoryPost alloc] init];
        catObj.categoryPostId = [lid integerValue];
        catObj.categoryPostType = type;
        catObj.categoryPostSlug = slug;
        catObj.categoryPostUrl = url;
        catObj.categoryPostStatus = lstatus;
        catObj.categoryPostTitle = title;
        catObj.categoryPostTitlePlain = title_plain;
        catObj.categoryPostContent = content;
        catObj.categoryPostExcerpt = excerpt;
        catObj.categoryPostDate = date;
        catObj.categoryPostModified = modified;
        [categoryArray addObject:catObj];
        catObj = nil;
        


    }
    
    [mainCategoryTable reloadData];
    
    self->responseData = nil;
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    self->responseData = nil;
	
}
- (void) logData 
{
    if(responseData != NULL)
	{
		NSString *result = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
		NSLog(@"\n result:%@\n\n", result);
    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [categoryArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    MadhuriCategoryPost *catObj = (MadhuriCategoryPost *)[categoryArray objectAtIndex:indexPath.row];
    cell.textLabel.text = catObj.categoryPostTitle;
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%d",indexPath.row);
    
    MadhuriWebViewController *typedetailController = [[MadhuriWebViewController alloc] initWithNibName:@"MadhuriWebViewController" bundle:nil] ;
    typedetailController.categoryId = indexPath.row;
    [self.navigationController pushViewController:typedetailController animated:YES];
    [mainCategoryTable deselectRowAtIndexPath:[mainCategoryTable indexPathForSelectedRow] animated:NO];
    
}


- (void)viewDidUnload
{
    [self setResponseIndicator:nil];
    [self setMainCategoryTable:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
