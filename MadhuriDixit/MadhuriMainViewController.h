//
//  MadhuriMainViewController.h
//  MadhuriDixit
//
//  Created by Rishi Saxena on 06/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MadhuriMainViewController : UIViewController {
    
}
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *latestBtnPressed;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *danceBtnPressed;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *artBtnPressed;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *fashionBtnPressed;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *healthBtnPressed;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *musicBtnPresswd;
@property (unsafe_unretained, nonatomic) IBOutlet UISwitch *networkSwitchPressed;
@property (nonatomic, readwrite) NSInteger networkstatus;


- (IBAction)loadLatestView:(id)sender;
- (IBAction)loadDanceView:(id)sender;
- (IBAction)loadArtView:(id)sender;
- (IBAction)loadFashionView:(id)sender;
- (IBAction)loadHealthView:(id)sender;
- (IBAction)loadMusicView:(id)sender;
- (IBAction)networkChanged:(id)sender;
@end
