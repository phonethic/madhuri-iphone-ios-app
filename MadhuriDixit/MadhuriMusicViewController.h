//
//  MadhuriMusicViewController.h
//  MadhuriDixit
//
//  Created by Rishi Saxena on 14/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MadhuriMusicViewController : UIViewController {
    NSMutableData *responseData;
    NSError  *connectionError;
    int status;
}

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *musicTableView;
@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *responseIndicator;
@property (strong, nonatomic) NSMutableArray *musicArray;
@property (strong, nonatomic) NSMutableDictionary *videodict;
@end
