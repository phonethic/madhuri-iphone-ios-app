//
//  MadhuriCategoryPost.h
//  MadhuriDixit
//
//  Created by Rishi Saxena on 04/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MadhuriCategoryPost : NSObject <NSCoding>

@property (nonatomic, readwrite) NSInteger categoryPostId;
@property (nonatomic, copy) NSString *categoryPostType;
@property (nonatomic, copy) NSString *categoryPostSlug;
@property (nonatomic, copy) NSString *categoryPostUrl;
@property (nonatomic, copy) NSString *categoryPostStatus;
@property (nonatomic, copy) NSString *categoryPostTitle;
@property (nonatomic, copy) NSString *categoryPostTitlePlain;
@property (nonatomic, copy) NSString *categoryPostContent;
@property (nonatomic, copy) NSString *categoryPostExcerpt;
@property (nonatomic, copy) NSString *categoryPostDate;
@property (nonatomic, copy) NSString *categoryPostModified;
@property (nonatomic, copy) NSString *categoryPostThumbnail;
@end
