//
//  MadhuriMovies.m
//  MadhuriDixit
//
//  Created by Rishi on 15/10/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "MadhuriMovies.h"

@implementation MadhuriMovies

@synthesize movieYear;
@synthesize moviename;
@synthesize movieImages;
@synthesize movieAwards;
@synthesize movieDirector;
@synthesize movieProducer;
@synthesize movieActedAs;
@synthesize movieAnecdotes;
@synthesize movieSynopsis;


@end
