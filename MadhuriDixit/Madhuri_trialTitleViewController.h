//
//  Madhuri_trialTitleViewController.h
//  Madhuri_trial
//
//  Created by Rishi on 14/09/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Madhuri_trialTitleViewController : UIViewController {
    int pageNumber;
    NSString *pressedimage;
    NSString *unpressedimage;
}
- (id)initWithPageNumber:(int)page unpressedImageName:(NSString *)pimage pressedImageName:(NSString *)unpimage;
@property (strong, nonatomic) IBOutlet UIButton *sliderButton;
@property (strong, nonatomic) IBOutlet UIImageView *sliderbackImage;



@end
