//
//  MadhuriMoviesViewController.m
//  MadhuriDixit
//
//  Created by Rishi on 17/09/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "MadhuriMoviesViewController.h"
#import "MadhuriMovies.h"
#import "MadhuriAppDelegate.h"
#import "MadhuriWebViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Flurry.h"

#define MOVIES_LINK @"http://madhuridixit-nene.com/uploads/filmography.xml"
//#define MOVIES_LINK @"http://localhost:81/filmography.xml"

@interface MadhuriMoviesViewController ()

@end

@implementation MadhuriMoviesViewController
@synthesize loaderImage;
@synthesize moviesTableView;
@synthesize moviesArray;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setupStrings];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // [self performSelector:@selector(playSectionAudio) withObject:nil afterDelay:2];
    [self addPullToRefreshHeader];
    // Do any additional setup after loading the view from its nib.
    [self rotateImage:loaderImage];
    
    if(moviesArray == nil) {
        moviesArray = [[NSMutableArray alloc] init];
    } else {
        [self.loaderImage.layer removeAnimationForKey:@"360"];
        [self.loaderImage setHidden:TRUE];
        goBack=YES;
        return;
        //[moviesArray removeAllObjects];
    }
    
    if([myAppDelegate networkavailable])
        [self projectListAsynchronousCall];
    else
    {
        goBack=YES;
        [self.loaderImage.layer removeAnimationForKey:@"360"];
        [self.loaderImage setHidden:TRUE];
        [self parseFromFile];
    }
}
-(void) playSectionAudio
{
    if (sectionSoundAudioPlayer == nil)
    {
        
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"aajnachle" ofType:@"mp3"]];
        sectionSoundAudioPlayer= [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [sectionSoundAudioPlayer setDelegate:self];
        [sectionSoundAudioPlayer setVolume:1.0];
    }
    sectionSoundAudioPlayer.currentTime = 0;
    [sectionSoundAudioPlayer play];
}
-(void) stopSectionAudio
{
    [sectionSoundAudioPlayer pause];
    [sectionSoundAudioPlayer stop];
}

-(void)projectListAsynchronousCall
{
	/****************Asynchronous Request**********************/
    goBack=NO;
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:MOVIES_LINK] cachePolicy:YES timeoutInterval:10.0];
    
	// Note: An NSOperation creates an autorelease pool, but doesn't schedule a run loop
	// Create the connection and schedule it on a run loop under our namespaced run mode
	[[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
	
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[self connectionDidFinishLoading:nil];
    //[self parseFromFile];
    //DebugLog(@"FAIL");
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	
	NSString *xmlDataFromChannelSchemes;
    if(isLoading && moviesArray != nil)
    {
        [self stopLoading];
        [moviesArray removeAllObjects];
    }

	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		//DebugLog(@"\n result:%@\n\n", result);
        [myAppDelegate writeToTextFile:result name:@"movies"];
		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
	} else {
        [self parseFromFile];
    }
    
    [self.loaderImage.layer removeAnimationForKey:@"360"];
    [self.loaderImage setHidden:TRUE];
    responseAsyncData=nil;
}

-(void) parseFromFile
{
    NSString *xmlDataFromChannelSchemes;
    NSString *data;
    data = [myAppDelegate getTextFromFile:@"movies"];

    //DebugLog(@"\n data:%@\n\n", data);

    if(![data isEqualToString:@""])
    {
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
        NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
        [xmlParser setDelegate:self];
        [xmlParser parse];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
    
    
    
}

#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"movies"])
	{
		moviesArray = [[NSMutableArray alloc] init];
	} else if([elementName isEqualToString:@"movie"]) {
        moviesObj = [[MadhuriMovies alloc] init];
        //DebugLog(@"%@", [attributeDict objectForKey:@"Name"]);
        moviesObj.movieYear = [attributeDict objectForKey:@"Year"];
        moviesObj.moviename = [attributeDict objectForKey:@"Name"];
        moviesObj.movieImages = [attributeDict objectForKey:@"Images"];
        moviesObj.movieAwards = [attributeDict objectForKey:@"Awards"];
        moviesObj.movieDirector = [attributeDict objectForKey:@"Director"];
        moviesObj.movieProducer = [attributeDict objectForKey:@"Producer"];
        moviesObj.movieActedAs = [attributeDict objectForKey:@"Acted_As"];
        moviesObj.movieAnecdotes = [attributeDict objectForKey:@"Anecdotes"];
        moviesObj.movieSynopsis = [attributeDict objectForKey:@"Synopsis"];
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"movies"])
    {
        //DebugLog(@"%d",[moviesArray count]);
        NSSortDescriptor *sortDescriptor =  [[NSSortDescriptor alloc] initWithKey:@"movieYear" ascending:YES];
        [moviesArray sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
        [moviesTableView reloadData];
    }
    else if([elementName isEqualToString:@"movie"])
    {
        [moviesArray addObject:moviesObj];
        moviesObj = nil;
    }
}



- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    //DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    //DebugLog(@"Error: %@", [validationError localizedDescription]);
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [moviesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UILabel *lblTitle;
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        //cell.backgroundColor = [UIColor clearColor];
        UIImageView *tempImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 80)];
        [tempImg setImage:[UIImage imageNamed:@"table_row_bg.png"]];
        cell.backgroundView = tempImg;
        
        UIImageView *tempselectedImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 80)];
        [tempselectedImg setImage:[UIImage imageNamed:@"table_row_bg_selected.png"]];
        cell.selectedBackgroundView = tempselectedImg;
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 50, 50)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.layer.cornerRadius = 10;
        thumbImg.layer.masksToBounds = YES;
        thumbImg.layer.borderColor = [UIColor lightGrayColor].CGColor;
        thumbImg.layer.borderWidth = 1.0;
        [cell.contentView addSubview:thumbImg];
        
        //Initialize Label with tag 2.(Title Label)
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 75, cell.frame.origin.y + 18, 220, cell.frame.size.height )];
        lblTitle.tag = 2;
        //lblTitle.shadowColor   = [UIColor blackColor];
        lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblTitle.font = TABLEVIEW_CELLLABEL_FONT;
        lblTitle.textAlignment = UITextAlignmentLeft;
        lblTitle.textColor = [UIColor blackColor];
        lblTitle.backgroundColor =  [UIColor clearColor];
        [self setShadow:lblTitle];
        [cell.contentView addSubview:lblTitle];
        
        //Initialize Image View with tag 3.(Arrow Image)
        /*      UIImageView *arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake(290, 24, 14, 12)];
         [arrowImg setImage:[UIImage imageNamed:@"bigarrow.png"]];
         arrowImg.tag = 3;
         [cell.contentView addSubview:arrowImg];*/
        
        
    }
    MadhuriMovies *tempmovieObj = (MadhuriMovies *)[moviesArray objectAtIndex:indexPath.row];
    UIImageView *thumbImgview = (UIImageView *)[cell viewWithTag:1];
    NSString *fileLink = [NSString stringWithFormat:@"%@/uploads/films/%@",MD_WEBSITE_LINK,tempmovieObj.movieImages];
    //DebugLog(@"%@",fileLink);
    [thumbImgview setImageWithURL:[NSURL URLWithString:fileLink]
                 placeholderImage:[UIImage imageNamed:@"Icon.png"]
                          success:^(UIImage *image) {
                              //DebugLog(@"success");
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                          }];
    
    lblTitle = (UILabel *)[cell viewWithTag:2];
    lblTitle.text = tempmovieObj.moviename;
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MadhuriMovies *tempmovieObj = (MadhuriMovies *)[moviesArray objectAtIndex:indexPath.row];
    MadhuriWebViewController *moviedetailController = [[MadhuriWebViewController alloc] initWithNibName:@"MadhuriWebViewController" bundle:nil] ;
    moviedetailController.title = tempmovieObj.moviename;
    moviedetailController.posttitle = [NSString stringWithFormat:@"<p><img class='aligncenter size-full wp-image-1607' title='' src=%@ alt='' width='' height='' /></p><p><center><U><H2>%@ (%@)</H2></U></center></p><p><strong>Director : </strong>%@</p><p><strong>Producer : </strong>%@</p><p><strong>Character : </strong>%@</p><p><strong>Summary : </strong>%@</p><p><strong>Synopsis : </strong>%@</p>\n",[NSString stringWithFormat:@"%@/uploads/films/%@",MD_WEBSITE_LINK,tempmovieObj.movieImages],tempmovieObj.moviename,tempmovieObj.movieYear,tempmovieObj.movieDirector,tempmovieObj.movieProducer,tempmovieObj.movieActedAs,tempmovieObj.movieSynopsis,tempmovieObj.movieAnecdotes];
    moviedetailController.categoryId = 1000;
    [self.navigationController pushViewController:moviedetailController animated:YES];
    [moviesTableView deselectRowAtIndexPath:[moviesTableView indexPathForSelectedRow] animated:NO];
    
}

-(void)setShadow:(UILabel *)label
{
    label.shadowColor = [UIColor whiteColor];
    label.shadowOffset = CGSizeMake(0.8, 0.0);
}
- (void)rotateImage:(UIImageView *)lview
{
    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = 1.00;
    fullRotation.repeatCount = HUGE_VALF;
    fullRotation.removedOnCompletion = YES;
    [lview.layer addAnimation:fullRotation forKey:@"360"];
    [lview.layer setSpeed:0.5];
    
    
}

- (IBAction)backActionMethod:(id)sender
{
    //[self stopSectionAudio];
    [Flurry endTimedEvent:@"Movies_Session" withParameters:nil];
    [UIView  transitionWithView:self.navigationController.view duration:1.0  options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController popViewControllerAnimated:YES];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

- (void)viewDidUnload
{
    [self setLoaderImage:nil];
    [self setMoviesTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//iOS 5
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
//iOS6
- (BOOL)shouldAutorotate {
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = REFRESH_LABEL_FONT;
    refreshLabel.textAlignment = UITextAlignmentCenter;
    refreshLabel.textColor = [UIColor whiteColor];
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [moviesTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (isLoading) return;
    isDragging = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            moviesTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            moviesTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
}

- (void)startLoading {
    isLoading = YES;
    
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        moviesTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([myAppDelegate networkavailable])
        [self projectListAsynchronousCall];
    else
        [self stopLoading];
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        moviesTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

//- (void)refresh {
//    // This is just a demo. Override this method with your custom reload action.
//    // Don't forget to call stopLoading at the end.
//    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:2.0];
//}


@end
