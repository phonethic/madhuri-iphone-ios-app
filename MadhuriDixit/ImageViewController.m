//
//  ImageViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 03/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ImageViewController.h"


@interface ImageViewController ()

@end

@implementation ImageViewController
@synthesize imgView;


- (id)initWithPageNumber:(int)page imageName:(NSString *)image height:(int)scrollheight {
    if (self = [super initWithNibName:@"ImageViewController" bundle:nil])
    {
        pageNumber = page;
        imageNameString=image;
        imageHeight=scrollheight;
        //   DebugLog(@"----- page init = %d and scrolltype is %@ %d \n",page ,imageNameString,imageHeight);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    imgView.image = [UIImage imageNamed:imageNameString];
    //[imgView setAccessibilityIdentifier:imageNameString] ;
    [imgView setTag:pageNumber];
    [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];

}

- (void)viewDidUnload
{
    imgView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//iOS 5
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
//iOS6
- (BOOL)shouldAutorotate {
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

@end
