//
//  MadhuriViewController.h
//  MadhuriDixit
//
//  Created by Rishi Saxena on 02/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MadhuriHealthViewController : UIViewController {
    NSMutableData *responseData;
    NSError  *connectionError;
    NSMutableArray *categoryArray;
    int status;
}
@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *responseIndicator;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *mainCategoryTable;
@property (strong, nonatomic) NSMutableArray *categoryArray;

@end
