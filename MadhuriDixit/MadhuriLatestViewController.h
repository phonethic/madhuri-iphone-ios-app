//
//  MadhuriLatestViewController.h
//  MadhuriDixit
//
//  Created by Rishi Saxena on 06/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface MadhuriLatestViewController : UIViewController <AVAudioPlayerDelegate> {
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    BOOL goBack;
    
    int status;
    
    BOOL isDragging;
    BOOL isLoading;
    BOOL showFooterView;

    AVAudioPlayer *sectionSoundAudioPlayer;

}

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *latestTableView;
@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *responseIndicator;
@property (strong, nonatomic) IBOutlet UIImageView *loaderImage;
@property (strong, nonatomic) NSMutableArray *latestArray;
@property (strong, nonatomic) IBOutlet UIView *alertView;
@property (nonatomic, readwrite) NSInteger categoryPostCount;
@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;
@property (nonatomic, copy) NSString *dataFilePath;
@property (nonatomic,strong)  UIView *footerView;

- (void)setupStrings;
- (void)addPullToRefreshHeader;
- (void)startLoading;
- (void)stopLoading;

@end
