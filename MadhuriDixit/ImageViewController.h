//
//  ImageViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 03/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController {
    int pageNumber;
    int imageHeight;
    NSString *imageNameString;
}

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
- (id)initWithPageNumber:(int)page imageName:(NSString *)image height:(int)scrollheight;
@end
